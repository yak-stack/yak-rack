# Yak Rack

The web front-end for Yak Stack. Likely to be developed after the Yak Tracker,
for now it will just be a light page for giving a little more info about the
Yak Stack.

Likely to be written in [KWeb](https://github.com/kwebio/kweb-core) for now. Probably React when we make it for real.
