package yakrack

import io.ktor.server.engine.EngineAPI
import io.kweb.Kweb
import io.kweb.dom.BodyElement
import io.kweb.dom.element.creation.tags.div
import io.kweb.dom.element.creation.tags.h1
import io.kweb.dom.element.creation.tags.h3
import io.kweb.dom.element.creation.tags.p
import io.kweb.dom.element.new
import io.kweb.plugins.fomanticUI.fomantic
import io.kweb.plugins.fomanticUI.fomanticUIPlugin


@EngineAPI
fun main(args: Array<String>) {
    Kweb(port = 9797, plugins = listOf(fomanticUIPlugin)) {
        doc.body.new {

            div(fomantic.ui.main.container).new {
                div(fomantic.column).new {
                    div(fomantic.ui.vertical.segment).new {
                        div(fomantic.ui.message).new {
                            p().innerHTML(
                                """
                                    <p>
                                    This project is in beta.
                                    </p>
                                    
                                    <p>
                                    By using it, you are subject to possible data loss and general instability.
                                    </p>
                                """
                                    .trimIndent()
                            )
                        }
                    }

                    div(fomantic.ui.vertical.segment).new {
                        h1(fomantic.ui.dividing.header).text("YakStack landing page")
                        h3(fomantic.ui.header).text("A suite of utilities for shaving yaks (tracking time and dealing with menial tasks)")
                        p().innerHTML("""
                            <p>
                            The YakStack is a suite of time tracking utilities, created for lack of a better
                            open source alternative.
                            </p>
                            <p> 
                            Initially given a subset of the functionality of Emacs Org mode and its timeclock.el
                            it is meant to seamlessly blend in with whatever your current workflow is.
                            </p>
                            <p>
                            Keep all of your tasks, their state, and the amount of time you've spent on all
                            of them in a backend so you can sync down your changes every time without using
                            something like Dropbox with text files. Built for handling times and tasks first,
                            the YakStack understands inputs from multiple clients better. You shouldn't
                            have to worry about getting inputs from two clients at once ever!
                            </p>
                            <p>
                            The YakStack is in beta and currently does not have any UIs that are out of
                            beta. The <a href="https://gitlab.com/yak-stack/yak-tracker">YakTracker</a>
                            is aimed at being the first fully featured desktop client, but still has much
                            more work to go before it's ready.
                            </p>
                        """.trimIndent())

                        h3(fomantic.ui.dividing.header).text("Developing for the YakStack")
                        p().innerHTML(
                            """
                                <p>
                                Looking to develop for the YakStack? We use GRPC only right now. You
                                can get the proto files <a href="https://gitlab.com/yak-stack/proto-yak">here</a>.
                                </p>
                                <p>
                                Once you've done this, you'll want an instance of the
                                <a href="https://gitlab.com/yak-stack/yaklet-webserver">Yaklet Webserver</a>
                                instructions for install are located there.
                                </p>
                                <p>
                                There are plans for supporting JSON as well as GRPC, but these plans haven't
                                come to fruition yet.
                                </p>
                            """.trimIndent()
                        )

                        h3(fomantic.ui.dividing.header).text("Hosted YakStack")
                        p().innerHTML(
                            """
                                <p>
                                A hosted version of the Yakstack is currently provided at
                                <a href="https://api.yakstack.dev">api.yakstack.dev</a> At the moment this is
                                my sort of personal instance, and if I notice use is being abused, data will be
                                deleted. Any use is subject to a privacy policy stating: <b>All data entered
                                is property of Darrien Glasser and he and any other contributors are allowed
                                to do whatever they please with it with no repercussions.</b> This privacy
                                policy may change at any time.
                                </p>
                                <p>
                                If you are from the EU, you are not allowed to use this currently. If
                                it is found you are from the EU or any country subject to GDPR, your
                                account and data may be deleted at any time.
                                </p>
                                <p>
                                It's worth noting this is not a production ready service, and this privacy
                                policy is provided so I (Darrien Glasser)can use the YakStack from anywhere.
                                When this is a production ready service, the terms will change to something
                                vastly more reasonable.
                                </p>
                            """.trimIndent()
                        )
                        h3(fomantic.ui.dividing.header).text("Can't wait for the YakStack?")
                        p().innerHTML(
                            """
                                <p>
                                Check out <a href="https://gitlab.com/ttyperacer/terminal-typeracer">Terminal Typeracer</a> in the mean time! Inspired by my girlfriend
                                Vrinda, Terminal Typeracer is a terminal typing game where you race to
                                get the fastest speed you can!
                                </p>
                                
                                <p>
                                To install, you can do a quick <i>cargo install typeracer</i> or check the
                                <a href="https://gitlab.com/ttyperacer/terminal-typeracer/-/releases">releases page</a> for binaries for Linux and macOS!
                                </p>
                            """.trimIndent()
                        )
                        h3(fomantic.ui.dividing.header).text("Need to contact?")
                        p().innerHTML(
                            """
                                I'm Darrien Glasser, and am available at <a href="mailto:contact@darrien.dev">contact@darrien.dev</a>
                                For more links information you may or may not care about, you can also check
                                out <a href="https://darrien.dev">https://darrien.dev</a>.
                            """.trimIndent()
                        )
                    }
                }
            }
        }
    }
}

